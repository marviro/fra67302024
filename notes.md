
Un peu de math https://mathvault.ca/hub/higher-math/math-symbols/

$$\pi$$

https://www.cmor-faculty.rice.edu/~heinken/latex/symbols.pdf

\[ \sum_{i=1}^{\infty} \frac{1}{n^s}
= \prod_p \frac{1}{1 - p^{-s}} \]


$$P(|E|, e_1^t)$$

$$P(E)=\prod_{t=1}^{T+1} P(e_1 | e_1^{t-1}$$



choses à rajouter:

- hypothèse distributionnelle avec exemples (cf note tiberio et note jadt)
- modèle inductif et déductif avec exemples (x+y = y+x, par ex et puis les cas particuliers)



<!--
Un peu de théorie générale:
  - numérique et modèles
  - turing
  - sens et syntaxe
Un minimum de compétences techniques:
  - littératie num de base: encodage du texte, machine de turing, langages de programmation
Un peu d'histoire:
  - ia 1 et 2 et 3 - Turing et l'idée d'aprentissage. Les systèmes experts, l'aprentissage machine et l'apretissage profond

  https://www.noemamag.com/what-ai-can-tell-us-about-intelligence/

  - un perceptron: regression, activatio function, loss function etc.
  - concepts importants à aborder: neurone, inputs, outputs, poids, biais, learning rate, gradient descent, activation function, temperature, cost (loss) function
  
Les LLM:
  - les hypothèses théoriques hypothèse distributionnelle, bayes et eulère
  - les fondements techniques 1 et 2 perceprton, word embedding, neural networks
Questions épistémologiques:
  - déduction vs induction (Chomsky etc.)
  - déduction vs induction IEML et Lévy

Questions politiques:
  - IA vs MDI
  - tesler et la course homme machine
  - IAL
  - plagiat et originalité
  - concentration
  - biais

softmax: https://alpslab.stanford.edu/papers/FrankeDegen_submitted.pdf  et https://www.youtube.com/watch?v=ytbYRIN0N4g

temperature

-->
## 17-10 Une introduction à l'IA: les concepts de bases des modèles d'apprentissage machine
 softmax, temperature, réseaux de neurones (prendre l'ancien) etc.
 - sequence2sequence model
 - encode/decode
 - probabilités et statistiques - probabilités = 0?
 - systèmes experts vs induction (séance dédiée après)

## 31-10 LLM: les fondements théoriques
  
- les hypothèses théoriques hypothèse distributionnelle, bayes et eulère

Probabilistic LM: https://homes.cs.washington.edu/~nasmith/papers/plm.17.pdf

la notion de LM cf neubig p 4 et 5: P(E) = P(|E|=T, e t/1) signifie: la probabilité de e qui a valeur cardinale (||) = à T, composé de phrases qui vont de e1 à et

n-gram et Markov assumption

log-linear models = features

 byte-pair encoding

## 07-11 Les LLM: quelques principes techniques

- word embedding (reprendre)
- tranformers
- Modèles de langue (regarder  tutoriel "neural machine translation") - et parler des systèmes P(e)

## 14-11 Les LLM: questions épistémologiques

  - déduction vs induction (Chomsky etc.)
  - déduction vs induction IEML et Lévy

  on a jeté à la poubelle même les choses de badse! tokenisation! regarde neubig et la question des règles syntaxiques.

  RAG!

  la notion de stochastique

https://ainowinstitute.org/wp-content/uploads/2023/04/AI-Now-2023-Landscape-Report-FINAL.pdf#page3
## 21-11 Questions théoriques: IA et modèles de définition de l'intelligence
  - tesler et la course homme machine
  - IAL
