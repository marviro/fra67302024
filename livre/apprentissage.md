# Les concepts de bases des modèles d’apprentissage machine

## Pourquoi apprendre?

L'idée de Turing et la métaphore de l'enfant.

- La complexité des règles
- L'ignorance des règles
- L'inexistance des règles? Est-ce qu'il y a toujours des règles?

## Types d'aprentissage

- supervisé
- non supervisé
- apprentissage par renforcement

### Supervisé

On a un jeu de données d'apprentissage de N couple de input-output:

$$(x_1,y_1),(x_2,y_2),...(x_N,y_N)$$

et chaque couple est produit par une fonction inconnue 

$$y=f(x)$$

L'objectif est de trouver une foction _h_ qui se rapproche le plus possible de _f_.

Normalement on a:

- un jeu d'aprentissage - des données annotées - ce qu'on appelle la vérité de terrain.
- un jeu de test - des données annotées qu'on n'utilise pas pour l'aprentissage, mais qui serviront à évaluer l'aprentissage



### Non supervisé

L'algorithme apprend en regardant des données non annotées. Un exemple est celui du _clustering_.

```{admonition} Exemple

- Un algorithme d'apprentissage non supervisé peut être utilisé pour regrouper des articles en différents groupes thématiques, même s'il n'a aucune connaissance préalable des sujets traités.
- Par exemple, il pourrait regrouper tous les articles sur la finance ensemble, tous les articles sur le sport ensemble, et ainsi de suite.
- L'algorithme identifie les similitudes entre les documents en analysant les mots et les expressions utilisés, puis regroupe les documents similaires.
- L'utilisateur n'a pas besoin de fournir d'exemples étiquetés au préalable. L'algorithme apprend de manière autonome à partir des données non structurées.

```

### Par renforcement

L'algorithme apprend sur la base des récompenses ou punitions qu'on lui donne à la fin du processus. 


## D'où viennent les règles?

- Sans théorie?
- Le choix des données 

```{admonition} Exemple
Choisir les données
```

| ID Patient | Âge | Sexe  | Tabagisme (paquets-ans) | Antécédents familiaux (oui/non) | Exposition professionnelle (oui/non) | Indice de masse corporelle (IMC) | Niveau de pollution (indice) | Symptômes (oui/non) |
|------------|-----|-------|--------------------------|----------------------------------|-------------------------------------|----------------------------------|------------------------------|---------------------|
| 1          | 65  | M     | 40                       | oui                              | oui                                 | 25                               | 7                            | oui                 |
| 2          | 54  | F     | 10                       | non                              | non                                 | 30                               | 5                            | non                 |
| 3          | 72  | M     | 60                       | oui                              | oui                                 | 22                               | 8                            | oui                 |
| 4          | 45  | F     | 0                        | non                              | non                                 | 28                               | 6                            | non                 |
| 5          | 58  | M     | 20                       | oui                              | oui                                 | 27                               | 9                            | oui                 |
| 6          | 66  | F     | 15                       | non                              | oui                                 | 26                               | 7                            | non                 |
| 7          | 50  | M     | 30                       | oui                              | non                                 | 29                               | 4                            | oui                 |
| 8          | 62  | F     | 5                        | non                              | oui                                 | 24                               | 6                            | non                 |
| 9          | 70  | M     | 45                       | oui                              | oui                                 | 23                               | 8                            | oui                 |
| 10         | 40  | F     | 2                        | non                              | non                                 | 31                               | 5                            | non                 |

Cet exemple propose des valeurs dont on sait qu'elles sont significatives par rapport au cancer. 

Une autre approche est de prendre le plus de valeurs possibles et de faire trouver à l'algorithme des patterns qu'on ne connaît pas forcement.

| ID Patient | Âge | Sexe  | Nombre de livres lus par an | Pratique de méditation (heures/semaine) | Temps passé sur les réseaux sociaux (heures/jour) | Fréquence de voyage (fois/an) | Couleur préférée | Utilisation de café (tasses/jour) | Animal de compagnie (oui/non) |
|------------|-----|-------|----------------------------|----------------------------------------|--------------------------------------------------|-------------------------------|------------------|-----------------------------------|-------------------------------|
| 1          | 65  | M     | 10                         | 2                                      | 3                                                | 1                             | bleu             | 2                                 | oui                           |
| 2          | 54  | F     | 5                          | 0                                      | 4                                                | 3                             | rouge            | 1                                 | non                           |
| 3          | 72  | M     | 20                         | 1                                      | 1                                                | 2                             | vert             | 0                                 | oui                           |
| 4          | 45  | F     | 15                         | 3                                      | 2                                                | 0                             | jaune            | 3                                 | non                           |
| 5          | 58  | M     | 3                          | 0                                      | 5                                                | 1                             | violet           | 4                                 | oui                           |
| 6          | 66  | F     | 8                          | 2                                      | 1                                                | 4                             | orange           | 1                                 | non                           |
| 7          | 50  | M     | 12                         | 1                                      | 2                                                | 2                             | bleu             | 0                                 | oui                           |
| 8          | 62  | F     | 0                          | 3                                      | 3                                                | 5                             | vert             | 2                                 | non                           |
| 9          | 70  | M     | 7                          | 1                                      | 4                                                | 1                             | rouge            | 5                                 | oui                           |
| 10         | 40  | F     | 18                         | 0                                      | 0                                                | 2                             | jaune            | 1                                 | non                           |


## Un monde inconnu

La plupart du temps nous avons à faire à des situations pour lesquelles nous n'avons pas de certitude.

- Pourquoi mon ordinateur ne s'allume pas?
- Est-il préférable de se laver les dents avant ou après le petit-déjeunner?
- Est-ce que le mal de tête recurrent que j'ai est signe d'un cancer au cerveau?

## Calculer la probabilité

L'idée de la distribution.

La règle de Bayes:

$$P(b|a) = \frac{P(a|b)\cdot P(b)}{P(a)}$$


### Problème

Est-ce que le mal de tête recurrent que j'ai est signe d'un cancer au cerveau?

### Notation

- Soit \( C \) l'événement "la personne a un cancer du cerveau".
- Soit \( H \) l'événement "la personne a des maux de tête".

Nous avons les informations suivantes :

- \( P(C) = 0.01 \) (probabilité d'avoir un cancer du cerveau, par exemple, 1%)
- \( P(H | C) = 0.90 \) (probabilité d'avoir des maux de tête si la personne a un cancer du cerveau)
- \( P(H) = 0.80 \) (probabilité d'avoir des maux de tête)

### Application de la règle de Bayes

Nous voulons calculer \( P(C | H) \) (probabilité d'avoir un cancer du cerveau sachant que la personne a des maux de tête).

La règle de Bayes s'énonce comme suit :

$$
P(C | H) = \frac{P(H | C) \cdot P(C)}{P(H)}
$$



$$
P(C | H) = \frac{P(H | C) \cdot P(C)}{P(H)} = \frac{0.90 \cdot 0.01}{0.80}
$$

Calculons :

$$
P(C | H) = \frac{0.009}{0.800} = 0.0113
$$

Je ne risque donc pas grand chose...


## Mettre à jour ses hypothèses

Utilisons la phrase suivante : "Mon animal de compagnie se déplace lentement et adore la laitue."

Utilisons la statistique bayesienne pour calculer, au fur et à mesure que nous construisons la phrase, la probabilité d'un de ces 3 mots:

1. chien
2. chat
3. tortue

Au début nous pouvons fixer les probabilités des trois mots comme étant 

$$
\frac{1}{taille du dictionnaire}
$$

Admettons que nous considérons 60.000 mots français. Les probabilités des 3 mots seront:

$$
\frac{1}{60.000}
$$

Donc:

- P(chat) = 0,0000167
- P(chien) = 0,0000167
- P(tortue) = 0,0000167

Maintenant nous voulons calculer la probabilité qu'un de ces mots acquiert dans le contexte où il y aura d'autres mots.

Avec la règle de Bayes:

$$
P(mot|contexte) = \frac{P(contexte|mot) \cdot P(mot)}{P(contexte)}
$$

Commençons notre phrase, avec le début de la phrase "mon animal de compagnie". Il nous faudra des données sur la fréquence de ce contexte avec les trois mots chien, chat et tortue. Admettons qu'elle soit équivalente:

Admettons:

- P("Mon animal de compagnie" | chat) = 0,1
- P("Mon animal de compagnie" | chien) = 0,1
- P("Mon animal de compagnie" | tortue) = 0,05

Admettons que la probabilité de la phrase "mon animal de compagnie" soit:

P(contexte) = 0,000001

Nous pouvons maintenant mettre à jour notre probabilité:

- P(chat | contexte) = (0,1 * 0,0000167) / 0,000012 ≈ 0,1392
- P(chien | phrase) = (0,1 * 0,0000167) / 0,000012 ≈ 0,1392
- P(tortue | phrase) = (0,05 * 0,0000167) / 0,000012 ≈ 0,0696

Mise à jour 2 : "se déplace lentement"

On prend comme probabilité initiale celle de la dernière mise à jour:

- P(chat) = 0,1392
- P(chien) = 0,1392
- P(tortue) = 0,0696

Et admettons que la probabilité du nouveau contexte "se déplace lentement" soit:

- P(contexte) = 0,111

Et que:

- P("se déplace lentement"|chat) = 0,1
- P("se déplace lentement"|chien) = 0,1
- P("se déplace lentement"|tortue) = 0,2


Calculs :

- P(chat | contexte) = (0,01 * 0,1392) / 0,111 ≈ 0,1254
- P(chien | contexte) = (0,001 * 0,1392) / 0,111 ≈ 0,1254
- P(tortue | contexte) = (0,2 * 0,0696) / 0,111 ≈ 0,1254

Maintenant une autre mise à jour, avec les mots "et adore la laittue".

Nous avons actuellement les probabilités suivantes:

- P(chat) = 0,1254
- P(chien) = 0,1254
- P(tortue) = 0,1254

Admettons que notre

- P(contexte) = 0,05

Et que :

- P("adore la laittue"|chat) = 0,01
- P("adore la laittue"|chien) = 0,02
- P("adore la laittue"|tortue) = 0,2

Nous allons avoir :

- P(chat | contexte) = (0,01 * 0,1254) / 0,05 ≈ 0,02508
- P(chien | contexte) = (0,02 * 0,1254) / 0,05 ≈ 0,05016
- P(tortue | contexte) = (0,2 * 0,1254) / 0,05 ≈ 0,5016

Avec ce contexte Les probabilités sont de 50% pour le mot tortue, 5% pour le mot chien et 2% pour le mot chat.

## Utiliser cette approche pour un modèle de langage mot-à-mot

L'objectif d'un modèle de langage est de calculer la probabilité d'une phrase E = e1, ..., eT. Pour simplifier le calcul, la probabilité d'une phrase complète est souvent décomposée en un produit de probabilités de mots individuels, conditionnées aux mots précédents.

Cela peut être formalisé comme suit:

$$P(E) = \prod_{t=1}^{T+1} P (e_t | e_1^{t-1})$$

où :
- P(E) représente la probabilité de la phrase E.
- $$\prod_{t=1}^{T+1}$$ 

indique le produit des probabilités de chaque mot de la phrase, du premier (t=1) au dernier (t=T+1).

- $$P(e_t | e_{t-1})$$ 

est la probabilité conditionnelle du mot $e_t$, étant donné tous les mots précédents dans la phrase 

$$(e_{t-1}, e_{t-2}, ..., e_1)$$

- $$e_{T+1} = 〈/s〉$$ 

`</s>` est un symbole spécial de fin de phrase, qui permet de déterminer la longueur finale de la phrase T.

Cette formule illustre l'idée que la probabilité d'une phrase est calculée en multipliant les probabilités de chaque mot, en tenant compte du contexte des mots précédents.

