# LLM: questions épistémologiques

Sources:


- Pierre Lévy, « Représentation Des Connaissances et Interopérabilité Sémantique Avec IEML », Pierre Levy’s Blog (blog), 19 avril 2024, https://pierrelevyblog.com/2024/04/19/representation-des-connaissances-et-interoperabilite-semantique-avec-ieml/.
- Jacob Browning, « What AI Can Tell Us About Intelligence », 16 juin 2022, https://www.noemamag.com/what-ai-can-tell-us-about-intelligence;
- Peter Norvig, « On Chomsky and the Two Cultures of Statistical Learning », dans Berechenbarkeit der Welt? Philosophie und Wissenschaft im Zeitalter von Big Data, éd. par Wolfgang Pietsch, Jörg Wernecke, et Maximilian Ott (Wiesbaden: Springer Fachmedien, 2017), 61‑83, https://doi.org/10.1007/978-3-658-12153-2_3;


## Qu'est-ce que la "science"?

- La notion de "succès"
- L'application
- Les résultats
- La prévision
- L'explication
- La compréhension
- Le sens?


>There is a notion of success ... which I think is novel in the history of science. (Chomsky, cité par Norvig)

>Breiman is inviting us to give up on the idea that we can uniquely model the true underlying form of nature's function from inputs to outputs. Instead he asks us to be satisfied with a function that accounts for the observed data well, and generalizes to new, previously unseen data well, but may be expressed in a complex mathematical form that may bear no relation to the "true" function's form (if such a true function even exists). Chomsky takes the opposite approach: he prefers to keep a simple, elegant model, and give up on the idea that the model will represent the data well. Instead, he declares that what he calls performance data—what people actually do—is off limits to linguistics; what really matters is competence—what he imagines that they should do. 
  
## Déduction et induction

- déduction: de l'universel au particulier
- induction: du particulier à l'universel



```{admonition} Exemple
:class: exemple

Déduction: `2x=y -> si x = 2 alors y=4`

Induction: `x = 1 et y = 2, x = 3 et y = 6 -> 2x=y` 


```

### Limites de l'induction

La dinde inductiviste de Popper: une dinde se lève chaque matin et son maître lui donne à manger; après plusieurs jours elle induit la règle universelle: mattin=repas. Jusqu'au jour de l'Action de grâce où son maître lui tire le cou...

Il faut beaucoup de données!

### Limites de la déduction

Est-ce qu'il y a toujours des règles universelles? Lesquelles?

Mais si on a la règle on n'a pas besoin de données!

## Connectivisme vs symbolisme

Le langage artificiel IEML de Pierre Lévy ([cf. notamment cet article](https://journals.openedition.org/revuehn/3836)) propose une représentation symbolique de la connaissance. C'est une représentation formelle où sémantique et syntaxe sont alignée par construction. La langue devient donc calculable.

- c'est une langue - avec les avantages de pouvoir "tout exprimer" - et non un système de règles
- elle permet d'exprimer des connaissances expertes et d'en déduire ensuite d'autres connaissances

## Des approches "mixtes"?

Le _Retrieval augmented generation_ (RAG) semble être une approche hybride.

Mais l'est-elle?

En réalité non, car il s'agit juste de donner des prompts pour "conditionner" le comportement du modèle inductif en aval - alors que pour avoir un système déductif il faudrait que la connaissance experte soit insérée en amont.

### Disantangled representation

Travailler de manière "experte" sur les couches cachées.

## Explicabilité, compréhension


## Dangers de la généralisation de l'induction

Un LLM utilise l'induction même quand il y a des règles simples

- [Clever Hans](https://en.wikipedia.org/wiki/Clever_Hans)  - les modèles inductifs souvent donnent le ["bon résultat pour la mauvaise raison"](https://github.com/cleverhans-lab/cleverhans?tab=readme-ov-file#about-the-name) - le blog [Cleverhans](https://www.cleverhans.io/security/privacy/ml/2016/12/16/breaking-things-is-easy.html)

>The story of Clever Hans is a metaphor for machine learning systems that may achieve very high accuracy on a test set drawn from the same distribution as the training data, but that do not actually understand the underlying task and perform poorly on other inputs.[Librairie cleverhans, Goodfellow et al](https://github.com/cleverhans-lab/cleverhans?tab=readme-ov-file)

- Byte pair encoding


On perd l'expertise!

Besoin de données et besoin de calcul = dépendance d'infrastructures très couteuses.

[Le rapport d'AInow](https://ainowinstitute.org/wp-content/uploads/2023/04/AI-Now-2023-Landscape-Report-FINAL.pdf)

## Juste une question de "performance"?

On dirait que dans le monde du développement d'algorithmes inductifs, les limites de l'induction sont juste compris comme des limites des performance et fiabilité.

Par exemple, l'approche de Papernot et Goodfellow consiste à se questionner sur la sécurité des modèles inductifs ([cf. ici](https://www.cleverhans.io/security/privacy/ml/2016/12/16/breaking-things-is-easy.html)). C'est ce qui les pousse à développer l'approche des "exemples adversaires" -- les contrexemples qui "cassent" le fonctionnement du modèle. 

Mais il faut considérer aussi le problème épistémologique!

## Stochastique

στόχος: "conjecture" (mais aussi "cible")

- quelque chose qui peut être décrit comme aléatoire _sur la base d'une distribution de probabilités_.

Admettons d'avoir un panier avec 5 billes rouges, 3 vertes et 2 jaunes. Si on prend une bille à l'aveugle du panier, on aura 50% de chances de prendre une bille rouge, 30% une verte et 20% une jaune. Le résultat est donc aléatoire (car c'est un tirage au sort) mais il reflètera la distribution de probabilité. Ce n'est donc pas comme si je prenais les billes dans un panier où il y avait 3 billes rouges, 3 vertes et 3 jaunes.



<!--
  - déduction vs induction (Chomsky etc.)
- déduction vs induction IEML et Lévy
-->
  <!-- on a jeté à la poubelle même les choses de badse! tokenisation! regarde neubig et la question des règles syntaxiques. -->

  <!-- RAG! -->

  <!-- la notion de stochastique -->

<!-- ## 21-11 Questions théoriques: IA et modèles de définition de l'intelligence -->
  <!-- - tesler et la course homme machine -->
  <!-- - IAL -->



<!-- - modèle inductif et déductif avec exemples (x+y = y+x, par ex et puis les cas particuliers) -->
