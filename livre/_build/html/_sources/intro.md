# Littérature et culture numérique 

Ce livre est le support de cours pour le séminaire "Littérature et culture numérique", FRA6730 donné à l'Université de Montréal à l'automne 2024 par [Marcello Vitali-Rosati](http://vitalirosati.com).

Vous trouverez ici l'ensemble des contenus du séminaire.

::: {important}

Les chapitres seront mis en ligne quelques jours avant la séance correspondante. Donc au début du cours seront disponibles seulement les premières séances.

:::


Cette année, le sujet du séminaire sont **les grands modèles de langage** (LLM).

## Description du séminaire

### Descriptif
Le séminaire propose une analyse théorique des changements culturels produits par le numérique en particulier dans le domaine de la littérature et des sciences humaines et sociales en général.

### Objectifs et contenu

L’objectif principal du séminaire est de développer une analyse théorique sur la manière dont le numérique – en tant que phénomène culturel et non en tant qu’ensemble d’outils techniques – change le statut de la littérature. Notre culture est en effet profondément bouleversée par le numérique qui modifie notre façon de percevoir l’espace, le temps, le rapport entre le domaine privé et le domaine public, notre identité et notre intimité.
Nous voulons interroger les pratiques numériques comme des révélateurs de ce qu’est la littérature, non seulement à notre époque, mais dans une dimension plus générale. En ce sens, le fait numérique ne sera pas analysé comme porteur de changements, mais comme un phénomène révélant de manière plus claire des aspects ontologiques qui, en tant que tels, ont une valeur atemporelle. Le séminaire a donc l’ambition de construire une théorie de la littérature à l’époque du numérique.

Cette année le séminaire se concentrera sur les Grands modèles de langage (Large langage models, LLM). Depuis quelques années, en effet, les algorithmes qui manipulent le langage naturel ont atteint des objectifs qui auraient pu sembler impossibles il y a peu de temps. Certains LLM semblent passer le test de Turing : il devient pratiquement impossible de distinguer leur capacité de manipuler la langue de celle d’un être humain. Mais que sont les LLM ? Sur quelles conceptions théoriques de la langue et du texte sont-ils fondés ? De quelle manière leur analyse peut-elle nous aider à développer des théories du texte, de la littérature, de l’originalité ? Ce séminaire abordera les fondements théoriques des LLM, en expliquera – de façon simplifiée – les principes techniques et proposera une réflexion sur les conséquences théoriques, politiques et esthétiques de leur rapide diffusion.


### Bibliographie
```{bibliography}
:all:
:style: plain
```


```{tableofcontents}
```
