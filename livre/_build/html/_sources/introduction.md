# Introduction 5-9-24


## Objectifs du séminaire 

-  S'interroger sur le numérique
-  S'interroger sur la culture numérique
-  S'interroger sur la littérature numérique
-  S'interroger sur la littérature à l'époque du numérique
-  Proposer une réflexion théorique sur la littérature en général
-  Proposer une réflexion théorique plus générale sur le monde actuel

## Le séminaire depuis 2015

-  2015: [littérature pornographique](http://vitalirosati.net/slides/cours-2015-fra6730.html#/)
-  2016: [littérature et réalité](http://vitalirosati.net/slides/cours-2016-fra6730.html#/)
-  2017: [littérature et espace](http://vitalirosati.net/slides/cours-2017-fra6730.html#/)
-  2018: [littérature et éditorialisation](http://vitalirosati.net/slides/cours-2018-fra6730.html#/)
-  2019: [l'écriture](http://vitalirosati.net/slides/2019/cours-2019-fra6730.html#/)
-  2020: [le code](http://vitalirosati.net/slides/2019/cours-2020-fra6730.html#/)
-  2021: [le texte](http://vitalirosati.net/slides/2021/fra6730/html/content.html)
-  2024: Les grands modèles de langage

Il y a une continuité entre ces thématiques: l'idée  est qu'il faut interpréter les environnements numériques en tant qu'espaces matériels dont les matériaux sont l'écriture et le texte.

## Les objectifs de cette année 



- Comprendre ce que sont les LLM
- Comprendre ce qu'on entend pas "Intelligence artificielle générative"
- S'interroger sur l'impact des LLM sur notre compréhension de la langue
- S'interroger sur l'impact des LLM sur notre compréhension du texte
- S'interroger sur l'impact des LLM sur notre compréhension de l'écriture
- S'interroger sur l'impact des LLM sur notre compréhension de la littérature et de l'art
- S'interroger sur la matérialité du texte et de la littérature




## Pourquoi les LLM?  

- Le discours médiatique
- La notion vague d'IA
- Les spécificités des LLM
- Questionner les lieux communs:
    - sur le rapport humain/machine
    - sur l'importance de l'"originalité"
    - sur l'importance de l'auteur
    - sur la séparation entre sens et syntaxe
- Le texte numérique
- Comprendre le code: la place des humanistes dans l'interprétation du monde numérique


## [Jupyter-notebook](introjupyternb.ipynb)

:::{important}

Toute la documentation ets disponible [ici](https://jupyter-notebook.readthedocs.io/en/stable/).

:::

:::{tip}

Vous devez apprendre à vous débrouiller! Cherchez, testez, bidouillez...

:::


## [Évaluation](evaluation)  

- Présentation du projet : 25-10-21
- Projet final 20-12-2021


## Devoirs pour cette semaine

1. Installer jupyter-notebook
2. Faire le [tutoriel markdown](https://www.markdowntutorial.com)
3. Produire un premier notebook avec un bloc de markdown et un bloc de code (un Hello world)
