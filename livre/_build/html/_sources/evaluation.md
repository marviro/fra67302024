# Évaluation

## Rendus  

- Présentation du projet (20%) : 24-10-24
- Projet final (80%) : 19-12-2024


## Le projet

::: {tip}

Les projets peuvent être individuels ou collectifs (groupes de 2, 3 ou plus personnes). 

:::

Le projet sera un jupyter-notebook qui propose des expérimentations réflexives sur les LLM. Par exemple:

- analyse des implications d'une composante algorithmique particulière des LLM 
- analyse d'une méthode ou d'une approche algorithmique particulière
- réflexion théorique sur une thématique prise en compte dans le séminaire à l'appui d'exemples de code
- expérimentation d'utilisation d'un modèle préentrainé pour réaliser une tâche spécifique
- ...


::: {tip}

N'hésitez pas à prendre un rdv pour discuter de vos idées!

:::



## Présentation du projet 20% (24-10-21)


Le texte doit présenter:

1. La nature du projet (en quoi consiste-t-il?)
2. La ou les questions théoriques soulevées
3. Les approches techniques et les méthodes qui seront utilisées 

Caractéristiques du texte:

- Entre 700 et 1300 mots
- Écrit en `markdown` + éventuellement `python`
- Dans un jupyter-notebook
- Déposé sur studium dans le format `.ipynb` **et** `.html`


## Projet final 80% 19-12-2024

Le projet final sera un jupyter-notebook qui contiendra:

1. La présentation du projet revue, corrigée et complétée
2. Le projet lui-même avec les expérimentations et le code



