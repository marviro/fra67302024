## Questions politiques: monopoles et biais

```{admonition} Source
:class: tip

- Emily M. Bender et al., « On the Dangers of Stochastic Parrots: Can Language Models Be Too Big? 🦜 », dans Proceedings of the 2021 ACM Conference on Fairness, Accountability, and Transparency, FAccT ’21 (New York, NY, USA: Association for Computing Machinery, 2021), 610‑23, https://doi.org/10.1145/3442188.3445922.
```

## Tout est une question de taille?

L'augmentation de la taille de ces modèles est souvent corrélée à une augmentation des performances, ce qui peut laisser croire que cette tendance va se poursuivre.

## À quel coût?

Une seule formation d'un modèle de base BERT peut consommer autant d'énergie qu'un vol transaméricain.

- impact environnemental

## Les minorités

- sous-représentée
- privée d'accès à des infrastructures trop grosses
- payent les coûts environnementaux

## Opacité

- La taille ne garantit pas la diversité
- Données statiques et opinions sociales en évolutio
- Biais!

## Compréhension? Ou perroquets stochastiques?



