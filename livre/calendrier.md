# Calendrier et lectures





## 05-09 Introduction

Présentation du séminaire: sujet, calendrier, lectures, évaluation.


## 12-09 Numérique, modélisation et algorithmes

```{admonition} À lire
:class: tip

- Jean-Guy Meunier, « Humanités numériques et modélisation scientifique », Questions de communication, nᵒ 31 (1 septembre 2017): 19‑48, https://doi.org/10.4000/questionsdecommunication.11040.
```

```{admonition} Pour aller plus loin...
:class: seealso

- Jean-Guy Meunier. Humanités numériques ou computationnelles : Enjeux herméneutiques. Sens public, Décembre 2014. URL: http://www.sens-public.org/article1121.html.
```

## 19-09 Une introduction à l'IA: les idées de Turing

```{admonition} À lire
:class: tip
- A. M. Turing. Computing Machinery and Intelligence. Mind, 59(236):433–460, October 1950. Oct., 1950. URL: http://www.jstor.org/stable/2251299 (visited on 2012-10-17). 


```
```{admonition} Pour aller plus loin...
:class: seealso

- Susan G. Sterrett, « Turing’s Two Tests for Intelligence », Minds and Machines 10, nᵒ 4 (1999): 541‑59.

```


## 26-09 Sens et syntaxe

```{admonition} À lire
:class: tip

- Stephen Marche. Literature Is not Data: Against Digital Humanities. October 2012. Section: essay. URL: https://lareviewofbooks.org/article/literature-is-not-data-against-digital-humanities/

- John R. Searle. Minds, brains, and programs. Behavioral and Brain Sciences, 3(3):417–424, 1980. Cambridge University Press. URL: https://www.cambridge.org/core/journals/behavioral-and-brain-sciences/article/minds-brains-and-programs/DC644B47A4299C637C89772FACC2706A, doi:10.1017/S0140525X00005756.
```

## 03-10 Une introduction à l'IA: les systèmes experts

```{admonition} À lire
:class: tip

- [Stuart J. Russell et Peter Norvig, Artificial intelligence: a modern approach, Fourth edition, Pearson series in artificial intelligence (Hoboken: Pearson, 2021)](https://matiere.ecrituresnumeriques.ca/index.php/s/eMQYALNNZ5zHcz4) - pp. 1-35.

```
## 10-10 Une introduction à l'IA: l'apprentissage machine illustré avec le perceptron

```{admonition} Pour aller plus loin
:class: seealso

- [Warren S. McCulloch et Walter Pitts, « A Logical Calculus of the Ideas Immanent in Nervous Activity », Bulletin of Mathematical Biology, 1943;](https://link.springer.com/content/pdf/10.1007/BF02478259.pdf)

- [F. Rosenblatt, The Perceptron, a Perceiving and Recognizing Automaton Project Para (Cornell Aeronautical Laboratory, 1957).](https://bpb-us-e2.wpmucdn.com/websites.umass.edu/dist/a/27637/files/2016/03/rosenblatt-1957.pdf)
```

## 17-10 Une introduction à l'IA: les concepts de bases des modèles d'apprentissage machine

```{admonition} Pour aller plus loin
:class: seealso

- Graham Neubig, « Neural Machine Translation and Sequence-to-Sequence Models: A Tutorial » (arXiv, 5 mars 2017), http://arxiv.org/abs/1703.01619.
- [Stuart J. Russell et Peter Norvig, Artificial intelligence: a modern approach, Fourth edition, Pearson series in artificial intelligence (Hoboken: Pearson, 2021)](https://matiere.ecrituresnumeriques.ca/index.php/s/eMQYALNNZ5zHcz4) - pp. 651 et ss.
```


## 24-10 semaine de lecture

```{admonition} Rendu
:class: danger

Déposez votre rendu sur Studium
```

## 31-10 LLM: les fondements théoriques
  
```{admonition} À lire
:class: tip

- Juan Luis Gastaldi et Luc Pellissier, « The Calculus of Language: Explicit Representation of Emergent Linguistic Structure through Type-Theoretical Paradigms », Interdisciplinary Science Reviews, 2 octobre 2021, https://www.tandfonline.com/doi/abs/10.1080/03080188.2021.1890484.


```


```{admonition} Pour aller plus loin...
:class: seealso

- John Rupert Firth, Studies in Linguistic Analysis, Repr, Special Volume of the Philological Society (Oxford: Blackwell, 1962); 
- Ferdinand de Saussure, Cours de linguistique générale (Paris: Payot, 1916), https://fr.wikisource.org/wiki/Cours_de_linguistique_g%C3%A9n%C3%A9rale.


```


## 07-11 Les LLM: quelques principes techniques


```{admonition} Pour aller plus loin...
:class: seealso
- Ashish Vaswani et al., « Attention is All You Need », 2017, https://arxiv.org/pdf/1706.03762.pdf.
- Timothy B. Lee, « Large Language Models, Explained with a Minimum of Math and Jargon », 6 mars 2024, https://www.understandingai.org/p/large-language-models-explained-with;
- Mourad Mars, « From Word Embeddings to Pre-Trained Language Models: A State-of-the-Art Walkthrough », Applied Sciences 12, nᵒ 17 (janvier 2022): 8805, https://doi.org/10.3390/app12178805;
- Usman Naseem et al., « A Comprehensive Survey on Word Representation Models: From Classical to State-of-the-Art Word Representation Language Models », ACM Transactions on Asian and Low-Resource Language Information Processing 20, nᵒ 5 (30 juin 2021): 74:1-74:35, https://doi.org/10.1145/3434237;
- Humza Naveed et al., « A Comprehensive Overview of Large Language Models » (arXiv, 20 février 2024), http://arxiv.org/abs/2307.06435.
```

## 14-11 Les LLM: questions épistémologiques

```{admonition} À lire
:class: tip
- Pierre Lévy, « Représentation Des Connaissances et Interopérabilité Sémantique Avec IEML », Pierre Levy’s Blog (blog), 19 avril 2024, https://pierrelevyblog.com/2024/04/19/representation-des-connaissances-et-interoperabilite-semantique-avec-ieml/.
- Jacob Browning, « What AI Can Tell Us About Intelligence », 16 juin 2022, https://www.noemamag.com/what-ai-can-tell-us-about-intelligence;
- Peter Norvig, « On Chomsky and the Two Cultures of Statistical Learning », dans Berechenbarkeit der Welt? Philosophie und Wissenschaft im Zeitalter von Big Data, éd. par Wolfgang Pietsch, Jörg Wernecke, et Maximilian Ott (Wiesbaden: Springer Fachmedien, 2017), 61‑83, https://doi.org/10.1007/978-3-658-12153-2_3;

```

```{admonition} Pour aller plus loin
:class: seealso
- Patrick Lewis et al., « Retrieval-Augmented Generation for Knowledge-Intensive NLP Tasks », dans Advances in Neural Information Processing Systems, vol. 33 (Curran Associates, Inc., 2020), 9459‑74, https://proceedings.neurips.cc/paper/2020/hash/6b493230205f780e1bc26945df7481e5-Abstract.html; 

```


## 21-11 Questions théoriques: IA et modèles de définition de l'intelligence

```{admonition} À lire
:class: tip

- Stephanie Dick, « Artificial Intelligence », Harvard Data Science Review 1, nᵒ 1 (3 juillet 2019), https://doi.org/10.1162/99608f92.92fe150c.
- Marcello Vitali-Rosati, « De l’intelligence artificielle aux modèles de définition de l’intelligence », Sens public, nᵒ 1722 (15 avril 2024), http://sens-public.org/articles/1729/.

```

## 28-11 Colloque Crihn sur les LLM

[Programme du colloque](https://www.crihn.org/nouvelles/2024/11/12/workshop-on-ai-dh-part-1/)

## 05-12 Originalité, créativité et fabrique des subalternes

```{admonition} À lire
:class: tip

- Vitali-Rosati, Marcello, « La Fabrique Des Subalternes: Les LLM, La Différence Homme-Machine et Le Mythe de l’originalité », Culture numérique. Pour une philosophie du numérique, 15 décembre 2023, http://blog.sens-public.org/marcellovitalirosati/fabrique-des-subalternes.html.

```

## 12-12 Questions politiques: monopoles et biais

```{admonition} À lire
:class: tip

- Emily M. Bender et al., « On the Dangers of Stochastic Parrots: Can Language Models Be Too Big? 🦜 », dans Proceedings of the 2021 ACM Conference on Fairness, Accountability, and Transparency, FAccT ’21 (New York, NY, USA: Association for Computing Machinery, 2021), 610‑23, https://doi.org/10.1145/3442188.3445922.
```


## 19-12 Rendu final

```{admonition} Rendu!
:class: danger

Rendu final!
```

