# IA ou modèles de définition de l'intelligence?


```{admonition} Sources
:class: tip

- Stephanie Dick, « Artificial Intelligence », Harvard Data Science Review 1, nᵒ 1 (3 juillet 2019), https://doi.org/10.1162/99608f92.92fe150c.
- Marcello Vitali-Rosati, « De l’intelligence artificielle aux modèles de définition de l’intelligence », Sens public, nᵒ 1722 (15 avril 2024), http://sens-public.org/articles/1729/.

```
Deux manières de regarder les approches computationnelles:

- les utiliser à des fins heuristiques
- les étudier car elle peuvent nous donner une compréhension hermeneutique de ce qu'elles décrivent (cf. projet IAL)

Ce que les LLM peuvent nous dire de la langue - ou de certains types d'"intelligence"

## Von Neuman et le réductionnisme

>You insist that there is something that a machine can’t do. If you will tell me precisely what it is that a machine cannot do, then I can always make a machine which will do just that.


## Knuth sur la science et l'art

>Science is knowledge which we understand so well that we can teach it to a computer; and if we don't fully understand something, it is an art to deal with it. Since the notion of an algorithm or a computer program provides us with an extremely useful test for the depth of our knowledge about any given subject, the process of going from an art to a science means that we learn how to automate something.[Knuth, Computer programming as an art](https://paulgraham.com/knuth.html)



## Le théorème de Tesler

- Selon Hofstadter (GEB 1979): 

>L'IA est tout ce qui n'a pas encore été fait

- Selon Tesler:

>L'intelligence est tout ce qu'une machine n'a pas encore fait


## Le bras de fer homme-machine 

- course pour rendre la machine plus intelligente?
- course de l'humain pour déplacer le sens de l'intelligence?


## La définition de l'humain 

>est 'humain' ce qui échappe à la machine



## Qu'est-ce que ne sait pas faire la machine?AI effect vs théorème de Tesler

- "intuition"
- "inspiration"
- "sentiments"
- langage naturel
- conscience

D'une part il y a l'idée  que l'IA court pour rattraper un humain qui continue de se déplacer. De l'autre l'idée d'un concept d'être humain qui court pour s'échapper de ce que savent faire les machines.



## ChatGPT

L'IA aurait comme objectif de reproduire artificiellement ce que l'IH serait "naturellement"

- Faire bien ce que les machines ne savent pas faire (parler)
- Faire mal ce que les machines savent faire (consulter des informations structurées)
- Le poids du Reinforcement learning


## Et si on renversait le modèle?:::


 En premier lieu mettre en question la question des frontières


## Posthuman studies 

>My posthumanist account calls into question the givenness of the differential categories of human and nonhuman, examining the practices through which these differential boundaries are stabilized and destabilized.
[Barad, Karen. 2007. Meeting the Universe Halfway: Quantum Physics and the Entanglement of Matter and Meaning. Second Printing edition. Durham: Duke University Press Books.]{.source}



## S'interroger sur la définition d'intelligence 

Comment définit-on l'intelligence?

Comment définit-on les différentes formes de comportement intelligent?

## Les modèles de définition de l'intelligence

- définir le langage naturel
- définir des concepts particuliers
- définir des sentiments
- définir la conscience 







## IEML

Information Economy MetaLanguage: un langage artificiel computable

- syntaxe régulière
- sens construit à partir de la syntaxe
- primitives, morphèmes, mots



IEML part d’éléments sémantiques atomiques - des “primitives”, semantic primitives . Il y en a six à partir desquels on construit ensuite 3200 “morphèmes” qui expriment des notions de base. Ce sont des atomes de sens à la fois assez universels et assez précis pour que, en les combinant, on puisse produire des éléments de sens plus complexes. Les mots et les phrases sont ensuite construits en combinant les morphèmes. Cela implique qu’un mot est fait d’un ensemble d’éléments sémantiques correspondant aux signes d’IEML: syntaxe et sémantique ne font qu’un. Le sens est donc calculable, mais, on pourrait aussi dire que la manipulation de la syntaxe correspond à la manipulation du sens. 



## Un concept en IEML


### L'exemple de "démocratie" {data-background="../img/levy.jpg" .fondoptitle}

![](http://vitalirosati.net/slides/img/phrase-ieml.png)

(exemple élaboré par Pierre Lévy [ici](https://pierrelevyblog.com/2023/02/20/comment-construire-un-concept-en-ieml-lexemple-de-la-democratie/))

```
@node
fr: démocratie
en: democracy
(
0 verbe: exercer le pouvoir
1 sujet: tous les citoyens
2 objet: unité politique / cité
4 cause/instrument: suffrage universel
7 intention/contexte: régime politique
8 manière: séparation des pouvoirs et protection des minorités
).
```



## Modéliser l'intelligence

- définitions non ambigües
- modélisation formelle
- compréhension de l'"intelligence"
- analyse des conceptions possibles de l'humain

