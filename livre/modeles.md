# Numérique, modélisation et algorithmes 12-9-24



## Un mot très utilisé

D'abord un adjectif, ensuite le mot est substantivé et commence à signifier un ensemble très hétérogène de choses. Il devient finalement une étiquette pour parler de notre époque.

```{admonition} Prudence...
:class: caution
Signification floue
```

On regroupe sous le chapeau de "numérique" un ensemble de choses, environnements, techniques, pratiques très différentes en les mettant toutes ensemble comme si elles étaient une unité.

```{admonition} Danger
:class: danger
Essentialisation
```

## Le numérique au sens propre du terme


Représentation de la réalité via des éléments discrets et atomiques qui correspondent à des nombres naturels.

::: {attention}
S’oppose à analogique: représentation du réel via un signal continu, “analogue” au réel.
:::

![Numérique et analogique](http://www.bedwani.ch/electro/ch23/image88.gif)


## La modélisation du monde

### Trois étapes:


- modèle représentationnel
   - donner une description en langage naturel de quelque chose
- modèle fonctionnel
   - transformer la représentation en unités atomiques discrètes et définir des fonctions pour les traiter 
    ```{attention}
    Le "_numérique_" se situe ici!
    ```
- modèle physique
   - implémenter le calcul fonctionnel dans une machine de Turing réelle.
   - calculable = computable

::: {attention}
Les trois étapes ne sont pas étanches!
:::


## Les “limites” de la machine?

- une machine ne peut pas…
- le topos de l’humain qui excède la machine
- l’angoisse de l’intelligence artificielle

## [Un exemple de modélisation simple](modelisation.ipynb)


## Les algorithmes

```{admonition} Définition

Ensemble d’instructions qui respectent deux conditions:

- à chaque instruction il est possible de connaître l’instruction suivante
- si on suit les étapes on arrive à une instruction qui demande l’arrêt.
```

## La machine de Turing

[Jouez avec une machine de Turing virtuelle](https://interstices.info/comment-fonctionne-une-machine-de-turing/)


## [Modèles et algorithmes](modelisationamour)

