# Originalité, créativité et fabrique des subalternes

```{admonition} Source
:class: tip

- Vitali-Rosati, Marcello, « La Fabrique Des Subalternes: Les LLM, La Différence Homme-Machine et Le Mythe de l’originalité », Culture numérique. Pour une philosophie du numérique, 15 décembre 2023, http://blog.sens-public.org/marcellovitalirosati/fabrique-des-subalternes.html.

```


## La peur

Une peur effrénée de voir menacée la place de “l’homme” dans le monde. Pourquoi?


>ce qui nous angoisse dans les LLM est le fait qu’ils semblent miner des stratégies discursives qui permettent de créer des dispositifs hiérarchiques où l’on définit de façon claire une séparation entre dominants et dominés. [MVR ici](https://blog.sens-public.org/marcellovitalirosati/fabrique-des-subalternes.html)

## Les caractéristiques d'une élite

- écrire de façon correcte
- manipuler la langue (style, rhétorique etc.)


>les stratégies discursives de définition de l’humain en opposition à quelque chose d’autre – comme les animaux, les automates ou les machines – sont de fait, toujours et avant tout, des stratégies de production de hiérarchies sociales, culturelles et, plus génériquement, symboliques qui peuvent être comprises comme une véritable fabrique des subalternes.

## À quoi attribuons-nous de la valeur symbolique?

## Matteo Pasquinelli et l'idée d'IA comme imitation de l'organisation du travail

Matteo Pasquinelli, _The eye of the master_, Verso, 2023


## Qui est menacé?

>The expansion in high-skill employment can be explained by the falling price of carrying out routine tasks by means of computers, which complements more abstract and creative services. [Osborne 2013](https://www.oxfordmartin.ox.ac.uk/downloads/academic/The_Future_of_Employment.pdf)

>But while expectations of the displacement of physical and manual work by machines has decreased, reasoning, communicating and coordinating – all traits with a comparative advantage for humans – are expected to be more automatable in the future. [Future of jobs report 2023](https://www3.weforum.org/docs/WEF_Future_of_Jobs_2023.pdf)


## 2 options

- trouver d'autres stratégies pour protéger les élites
- questionner la production de valeur symbolique


