# LLM: les fondements théoriques

```{admonition} 
:class: tip

- Juan Luis Gastaldi et Luc Pellissier, « The Calculus of Language: Explicit Representation of Emergent Linguistic Structure through Type-Theoretical Paradigms », Interdisciplinary Science Reviews, 2 octobre 2021, https://www.tandfonline.com/doi/abs/10.1080/03080188.2021.1890484.


```

## Des systèmes "intelligents"?

Non: des systèmes qui savent manipuler la langue comprise comme système clos, qui ne se réfère à rien.


Une théorie de la langue et non des agents linguistiques.

## Hypothèse distributionnelle

**la signification d'un mot est déterminée par, ou du moins fortement corrélée avec, les multiples contextes linguistiques dans lesquels ce mot apparaît**


- Hier il y avait du filurbilain au resto.
- Personne ne sait cuisiner le filurbilain comme ma mère
- Le filurbilain est très long à cuisiner
- Tu connais la recette du filurbilain?


Les mots qui précèdent ou qui suivent le mot nous font comprendre son sens.

Quel est le sens de ce mot?


## Hypothèse distributionnelle vs linguistique générative

Linguistique générative (Chomsky): la langue fonctionne avec des règles qui peuvent être comprises et modélisées. 

Hypothèse distributionnelle: la langue ne dépend que des relations internes à partir desquelles on peut induire un sens et un usage.

## Transformer les relations en vecteurs

## Relations entre les mots selon l'approche structuraliste

Les deux principales relations qui, selon les linguistes structuralistes, déterminent les unités linguistiques sont les relations syntagmatiques et les relations paradigmatiques.

- Les relations syntagmatiques sont celles qui constituent les unités linguistiques (par exemple, les mots) en tant que parties d'une séquence observable de termes (par exemple, des phrases ou des propositions). Autrement dit, ce sont les relations que les mots ont entre eux lorsqu'ils sont placés les uns à côté des autres dans une phrase. Par exemple, dans la phrase "Le chat dort sur le tapis", les mots "chat", "dort", "sur", "le" et "tapis" sont liés syntagmatiquement.
- Les relations paradigmatiques, quant à elles, sont des relations associatives que chaque unité linguistique entretient avec toutes les autres unités qui pourraient lui être substituées à une position particulière. En d'autres termes, ce sont les relations que les mots ont entre eux lorsqu'ils peuvent être interchangés dans une phrase sans changer la grammaticalité de la phrase. Par exemple, dans la phrase "Le chat dort sur le tapis", le mot "chat" entretient une relation paradigmatique avec des unités telles que "chien", "oiseau" ou "enfant", car ces mots pourraient tous être substitués à "chat" dans la phrase sans la rendre agrammaticale.

Du point de vue structuraliste, les propriétés des unités linguistiques sont déterminées au croisement de ces deux types de relations

## Modèle sequence to sequence

Un modèle séquence à séquence est un type de modèle d'apprentissage automatique qui apprend à mapper une séquence d'entrée vers une séquence de sortie

Les modèles séquence à séquence sont utilisés dans une variété de tâches de traitement du langage naturel, notamment :

- Traduction automatique : Traduction d'une phrase d'une langue à une autre.●
- Étiquetage : Attribution d'étiquettes à des mots dans une phrase, comme les noms ou les verbes.
- Dialogue : Génération de réponses dans un dialogue.
- Reconnaissance vocale : Conversion de la parole en texte.

![](sequence2sequence.png)

<!--
- les hypothèses théoriques hypothèse distributionnelle, bayes et eulère

Probabilistic LM: https://homes.cs.washington.edu/~nasmith/papers/plm.17.pdf

la notion de LM cf neubig p 4 et 5: P(E) = P(|E|=T, e t/1) signifie: la probabilité de e qui a valeur cardinale (||) = à T, composé de phrases qui vont de e1 à et

n-gram et Markov assumption

log-linear models = features

 byte-pair encoding

--> 
