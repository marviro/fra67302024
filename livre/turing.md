# Les idées de Turing 19-9-24

## Un peu d'histoire

L'intelligence artificielle n'est pas née hier!

On peut suivre plusieurs pistes:

1. La piste de la modélisation mathématique
2. La piste de l'automatisation
3. La piste de l'informatique

## Modélisation mathématique

- Depuis -- au moins -- Pythagore! (580-495 av.J.-C.)
- Al-Khwârizmî (780-850)
- Raymond Lulle (1232-1315)
- Pascal (1623-1662)
- Leibniz (1646-1716)
- ...

Pour ne faire que quelques exemples...

## Automatisation

- Archimède
- ...


## Informatique

- Léonard de Vinci (?)
- Pascal
- Turing
- McCarthy
- ...
- Bengio, Goodfellow, LeCun

## Intelligence artificielle?

Mais qu'est-ce que cela veut dire?

## Qu'est-ce que l'intelligence?






## Le test de Turing

![](http:vitalirosati.net/slides/img/Turing_test_diagram.png)

Turing, A. M. « Computing Machinery and Intelligence ». Mind LIX, nᵒ 236 (1 octobre 1950): 433‑60. [https://doi.org/10.1093/mind/LIX.236.433](https://doi.org/10.1093/mind/LIX.236.433).



## Une machine, peut-elle penser?

Il est très difficile de définir ce que signifie "penser".

Imitation game: homme et femme et ensuite machine:

- A est homme et essaie de tromper C

- B est femme et chercher d'aider C

## Les deux tests de Turing

- [Susan G. Sterrett, « Turing’s Two Tests for Intelligence », Minds and Machines 10, nᵒ 4 (1999): 541‑59.](http://philsci-archive.pitt.edu/8480/)


- Original Turing Test: la machine prend la place de l'homme, le but du jeu reste de faire croire que c'est une femme
  - il est possible que la machine fasse plus de points que l'homme, car on compare deux jeux (homme vs femme ou machine vs femme)
- Test simplifié: la machine prend une place, l'autre c'est un humain: le but du jeu est de reconnaître qui est la machine

## Et le test vulgarisé

On ne sait pas dire si la machine est une machine ou un humain.

## Que signifie penser?

## Que signifie "machine"?

## La reformulation du problème

>‘Let us fix our attention on one particular digital computer C. Is it true that by modifying this computer to have an adequate storage, suitably increasing its speed of action, and providing it with an appropriate programme, C can be made to play satisfactorily the part of A in the imitation game, the part of B being taken by a man?’

## Les arguments contre


## Des machines qui apprennent

## Le jeu des rôles - Imitation game

>_découvrir_ une différence ou la _performer_?

L'homme fait la femme. Il se comporte comme il croit qu'une femme DOIT se comporter. Donc il produit une certaine idée du genre féminin

On présuppose une différence et le but du jeu est justement de renforcer cette différence (le genre). On performe le genre


## Performer l'humain 

![](http://vitalirosati.net/slides/img/hommevitruviencoupe.jpg)


**Take away message**

>Dans le test de Turing on ne "découvre" pas qui est l'être humain et qui est la machine: on produit la définition de ce qu'est l'humain et de ce qu'est une machine.



