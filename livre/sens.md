# Sens et syntaxe  26-9-24


## McCarthy et le réductionnisme

- On peut tout modéliser
- On peut donc aussi modéliser l'humain
- L'humain fonctionne comme une machine

## Les problèmes du réductionnisme

- L'humain est quelque chose de plus, qu'on ne peut pas modéliser
- Ou plutôt: l'humain n'est pas quelque chose de défini... donc on ne peut pas le modéliser

Une autre idée de réductionnisme:

- On peut modéliser tout ce qui est défini de manière non ambigue

Qu'est-ce que cela veut dire?



## La chambre chinoise

![](http://vitalirosati.net/slides/img/Chinese-room-experiment.jpg)


Searle, John R. « Minds, Brains, and Programs ». Behavioral and Brain Sciences 3, nᵒ 3 (septembre 1980): 417‑24. https://doi.org/10.1017/S0140525X00005756.

>My car and my
adding machine, on the other hand, understand nothing: they
are not in that line of business.

Donc c'est une question d'essence: de la machine et de l'être humain. L'objectif n'est pas de savoir si la machine peut penser, mais de définir ce qu'est l'humain par opposition à quelque chose d'autre, dans ce cas une machine. Ce n'est pas seulement une opposition, par contre, mais aussi, et surtout, une hiérarchisation.


## Qui est le plus intelligent? 


- homme vs femme
- humain vs animal
- humain vs machine


>Instead of arguing continually over this point it is usual to have the polite convention that everyone thinks.

Turing, A. M. « Computing Machinery and Intelligence ». Mind LIX, nᵒ 236 (1 octobre 1950): 433‑60. [https://doi.org/10.1093/mind/LIX.236.433](https://doi.org/10.1093/mind/LIX.236.433).


## Le sens et la syntaxe 

- une table d'associations

|input|output|
|-----|------|
|`a?` | `b`  |

Une opération

```
a?
b
```


## Le sens et la syntaxe 

- une table d'associations

|input|output|
|-----|------|
|`Qu'est-ce que le soleil?` | `Une étoile`  |

Une opération

```
Qu'est-ce que le soleil?
Une étoile
```


Quelle est la différence entre ces deux exemples ? D'un point de vue syntaxique, il n'y a pas de différence du tout. Mais il y a une différence s'il n'y a plus de table de correspondance : une machine ne sera pas capable de combiner le premier symbole avec le second. Mais on croit qu'un humain sera capable d'aller au-delà de la syntaxe et de comprendre le sens, sans avoir besoin d'une table, il sera capable de combiner les deux. En effet, il "comprendra" ce que signifient "soleil" et "étoile". On pourra aussi dire que l'humain ressent quelque chose de différent quand il lit 'a ?' puis 'b' et quand il lit 'Qu'est-ce que le soleil ?' et ensuite 'une étoile'.




- Syntaxe = juste ce qu'on voit: table + symboles
- Sens = quelque chose de plus... mais quoi?

## Et si on complexifiait la table? 

![](http://vitalirosati.net/slides/img/soleil.png)




Et si ce quelque chose de plus était justement par ex ce qui nous échappe dans les matrices de réseaux de neurones? Justement parce qu'on ne sait pas, comme on ne sait pas ce que veut dire qu'un enfant comprend... Cf Michèle Sebag Goodfellow et Clever Hans. 


Le problème est que cet exemple est conçu pour définir clairement l'ensemble des éléments disponibles dans le premier cas et pour laisser ambigus les éléments disponibles dans le second cas.

Dans le premier cas, il n'y a que les deux symboles et le tableau. Dans le second, il y a autre chose. Mais qu'est-ce que ce quelque chose d'autre ?

La réponse que l'exemple tente de provoquer est : "le sens", en supposant que **le sens est ce qui reste après avoir considéré tous les éléments déclarés**. Mais le problème est que ces éléments n'ont pas été déclarés. 

Et s'il existait de nombreux tableaux, chacun définissant des instructions pour établir des relations entre différents symboles ? Une table établissant une relation entre le symbole Soleil et le symbole étoile, une autre établissant une relation entre Soleil et une image, une autre établissant une relation entre Étoile et lumineux, une autre entre lumineux et chaud et entre étoile et objet astronomique constitué d'un sphéroïde lumineux de plasma maintenu par sa propre gravité, et ainsi de suite. Imaginez des millions, des milliards, des trillions de ces relations. L'exemple serait-il encore valable ?


Quelque chose de semblable:


Tononi, Giulio. 2012. _Phi: A Voyage from the Brain to the Soul_. New York: Pantheon


## La définition de l'humain 

>est 'humain' ce qui échappe à la machine




## Posthuman studies 

>My posthumanist account calls into question the givenness of the differential categories of human and nonhuman, examining the practices through which these differential boundaries are stabilized and destabilized.
[Barad, Karen. 2007. Meeting the Universe Halfway: Quantum Physics and the Entanglement of Matter and Meaning. Second Printing edition. Durham: Duke University Press Books.]{.source}


## L'exemple de IAL

Une machine peut-elle penser ? Un algorithme peut-il saisir un concept littéraire ? À partir de définition formelle, nous construisons et entraînons des algorithmes capables d'identifier et de comprendre un concept littéraire à travers des textes. Dans le cadre du projet IAL, nous travaillons plus particulièrement sur la notion de 'variation' dans l'Anthologie Grecque : identification des variations (reprises, réécritures, reformulations) dans les épigrammes, définition formelle du concept, implémentation de la définition dans un algorithme. L'algorithme, capable de repérer sans faute toutes les variations précédemment identifiées comme telles, incarnera notre définition et 'comprendra' le concept de variation.


[Présentation du projet](http://vitalirosati.net/slides/2024/conf-2024-06-27jadt.html)
